import React from 'react';
import Sidebar from './components/SidebarMenu/Sidebar';
import './App.css';
import { BrowserRouter as Router, Switch } from 'react-router-dom';
import SidebarData from './components/SidebarMenu/SidebarData';
import RouteGenerator from './components/SidebarMenu/RouteGenerator';

function App() {

	return (
		<Router>
			<Sidebar />
			<Switch>
				{SidebarData && SidebarData.map((item, index) => {
					return <RouteGenerator item={item} key={index} />
				})}
			</Switch>
		</Router>
	);
}

export default App;
