import React from 'react'

const agent = ({title}) => {
    return (
        <div id={title}>
            <h1>{title}</h1>
        </div>
    )
}

export default agent
