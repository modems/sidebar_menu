import React from 'react'

const Dashboard = ({title}) => {
    return (
        <div id={title + ` Page`} className="flex h-screen items-center justify-center text-5xl">
            <h1>{title || 'walao'}</h1>
        </div>
    )
}

export default Dashboard
