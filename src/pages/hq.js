import React from 'react'

const hq = ({title}) => {
    return (
        <div>
            <h1>{title}</h1>
        </div>
    )
}

export default hq
