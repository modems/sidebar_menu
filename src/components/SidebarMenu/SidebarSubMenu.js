import React, { useState } from 'react'
import {Link} from 'react-router-dom';
import * as RiIcons from 'react-icons/ri';

const SidebarSubMenu = ({item, currentDepth, parentId}) => {

    const depth = currentDepth || 0;

    const pathTo = (parentId || '') + '/' + item.id;

    const [sidebarmenu, setSidebarSubMenu] = useState(false);
    
    const showSidebarSubMenu = () => setSidebarSubMenu(!sidebarmenu);

    return (
        <div className={"bg-gray-" + (900 - (depth * 100))}>
            <Link
                data-depth={depth}
                to={ pathTo } 
                className={ depth < 1
                    ? "flex justify-between items-center p-5 list-none h-16 no-underline text-xl border-r-4 border-black hover:bg-gray-500 hover:cursor-pointer hover:border-yellow-500"
                    : "flex justify-between items-center p-5 pl-" + (5 + (depth * 2)) + " h-14 no-underline text-lg border-r-4 border-black hover:bg-gray-500 hover:cursor-pointer hover:border-yellow-500"}
                onClick={ item.childs && showSidebarSubMenu}
            >
                <div>
                    <span className="text-white">
                        {capitalize(item.id)}
                    </span>
                </div>
                <div>
                    {item.childs && sidebarmenu
                        ? <RiIcons.RiArrowDownSLine className="text-white" />
                        : (item.childs
                            ? <RiIcons.RiArrowLeftSLine className="text-white" />
                            : null)
                    }
                </div>
            </Link>
            {sidebarmenu && item.childs.map((item, index) => {
                return <SidebarSubMenu item={item} key={index} currentDepth={depth+1} parentId={pathTo} />
            })}
        </div>
    )
}

const capitalize = (str) => {
    return str.charAt(0).toUpperCase() + str.slice(1);
}

export default SidebarSubMenu
