import React from 'react'
import {Link} from 'react-router-dom';
import SidebarData from './SidebarData';
import SidebarSubMenu from './SidebarSubMenu';
import avanaLogo from './avana_logo.png';
import profileImage from './profileImage.png';

const Sidebar = () => {

    return (
        <>
            <nav name="SidebarNav" className={`flex bg-gray-900 w-72 h-screen justify-center fixed top-0 left-0 transition-transform z-10 overflow-auto`}>
                <div name="SidebarWrap" className="w-full">
                    <Link name="NavIcon" to="/" className="flex justify-start items-center ml-8 text-4xl h-20">
                        <img src={avanaLogo} alt="" className="h-12 w-5/6 z-20" />
                    </Link>
                    <div name="profileImage">
                        <img src={profileImage} alt="" className="rounded-full p-10" />
                    </div>
                    {SidebarData.map((item, index) => {
                        return item.isShowed
                            ? <SidebarSubMenu item={item} key={index} currentDepth={0} />
                            : null;
                    })}
                </div>
            </nav>
        </>
    );
};

export default Sidebar
