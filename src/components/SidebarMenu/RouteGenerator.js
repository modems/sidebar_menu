import { Route } from 'react-router-dom';
import Dashboard from '../../pages/Dashboard';

const RouteGenerator = ({item, parentId}) => {

    // One way is to hardcode everything

    // return (
    //     <>
    //         <Route path="/dashboard" exact component={Pages.Dashboard} />
    //         <Route path="/hq" exact component={Pages.Hq} />
    //             <Route path="/hq/hq_stockist" exact component={Pages.HqStockist} />
    //             <Route path="/hq/hq_dropship_affiliate" exact component={Pages.HqDrophipAffiliate} />
    //         <Route path="/agent" exact component={Pages.Agent} />
    //             <Route path="/agent/my_purchase" exact component={Pages.MyPurchase} />
    //         <Route path="/orders" exact component={Pages.Orders} />
    //             <Route path="/orders/product-allproduct" exact component={Pages.ProductAllProduct} />
    //                 <Route path="/orders/product-allproduct/product-add" exact component={Pages.ProductAdd} />
    //                 <Route path="/orders/product-allproduct/product-import" exact component={Pages.ProductImport} />
    //             <Route path="/orders/product-categories" exact component={Pages.ProductCategories} />
    //             <Route path="/orders/product-variations" exact component={Pages.ProductVariations} />
    //             <Route path="/orders/product-collections" exact component={Pages.ProductCollections} />
    //         <Route path="/webstore" exact component={Pages.WebStore} />
    //             <Route path="/webstore/settings" exact component={Pages.WebStoreSettings} />
    //                 <Route path="/webstore/settings/webstore-domain" exact component={Pages.WebStoreSettingsWebStoreDomain} />
    //                     <Route path="/webstore/settings/webstore-domain/default-domain" exact component={Pages.WebStoreSettingsWebStoreDomainDefaultDomain} />
    //                     <Route path="/webstore/settings/webstore-domain/custom-domain" exact component={Pages.WebStoreSettingsWebStoreDomainCustomDomain} />
    //                     <Route path="/webstore/settings/webstore-domain/request-custom-domain" exact component={Pages.WebStoreSettingsWebStoreDomainRequestCustomDomain} />
    //     </>
    // )

    // TODO : This is not working. Need fixes
    const pathTo = (parentId || '') + '/' + item.id;

    const SelectedComponent = capitalize(item.id);

    return (
        <>
            <Route exact path={item.route} component={(props) => {
                return <SelectedComponent title={item.title} content={item.content} />
            }} />
            {item.childs && item.map((item, index) => {
                return <RouteGenerator item={item} key={index} parentId={pathTo} />
            })}
        </>
    );
}

const capitalize = (str) => {
    return str.charAt(0).toUpperCase() + str.slice(1);
}

export default RouteGenerator
