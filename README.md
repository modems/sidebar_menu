# Muhammad Norafif - Avana Test (FE)

To do : 
1. Run `npm install` to install all the dependencies.
2. Run `npm run start` to start the local server.
3. Open [`http://localhost:3000`](http://localhost:3000) to view it in the browser.


### Libraries used:
I used TailwindCss for the styling, craco to wrap tailwind to React.
Few React libraries such as react-router-dom, react-icons, etc.

### Notes:
I am not able to finish the routing part. Kinda stuck at RouteGenerator.js with the time given. Please ignore testing on that.

### Suggestion/Improvement that can be done:
- Can implement `fs` in javascript or get list of files in pages from API/BE to loop and generate routing automatically. This is better than hardcoding the files/components.
- More parts can be componenise (read:component+nise/nize) such as logo and profile picture (which can be use in another place including the RouteGenerator.
- Requirement for isAllowed not that clear (can be access to the page or access for the specific user. Not sure about this) but ya, can implement more logic on that
